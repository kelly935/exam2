#ifndef _RECURSION_H_
#define _RECURSION_H_

/** Namespace is here so that C++ compiler does not get confused 
 *  between functions with same names.
 */
namespace question_10
{
    int sumOfSquare(int n);
}

#endif