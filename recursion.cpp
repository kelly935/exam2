#include "recursion.h"
/** this is the answer to question number 10
 */

//Recursive Functions
int question_10::sumOfSquare(int n)
{
    //Error handling to avoid infinite loop
    if(n < 1)
    {
        return 0;
    }

    //Base case when n == 1
    if(n == 1)
    {
        return 1;
    }

    //We return the current number N^2 + the next recursive call where n = n-1
    return (n*n) + sumOfSquare(n-1);
}