#include "tailrecursion.h"

/** this is the answer to question number 11
 */

//Tail Recursive function
int sumOfSquareTail(int n, int val)
{
    //Error handling to avoid infinite loop
    if(n < 1)
    {
        return 0;
    }

    //Base case when n == 1
    if(n == 1)
    {
        return val+1;
    }

    //We return the current number N^2 + the next recursive call where n = n-1
    return sumOfSquareTail(n - 1, val+(n*n));
}

/** Helper function here so that we can still have the same function signature
 * while using a tail recursive function call, which requires an accumulator
 * argument (val) in order to make it work
 */
int question_11::sumOfSquare(int n)
{
    return sumOfSquareTail(n, 0);
}

