#include <iostream>
#include "recursion.h"
#include "tailrecursion.h"

int main()
{
    //This is testing the recursive fuction from question 10
    std::cout << question_10::sumOfSquare(5) << std::endl;

    //This is testing the tail recursive function from question 11
    std::cout << question_11::sumOfSquare(5) << std::endl;
}