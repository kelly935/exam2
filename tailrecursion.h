#ifndef _TAIL_RECURSION_H_
#define _TAIL_RECURSION_H_

/** Namespace is here so that C++ compiler does not get confused 
 *  between functions with same names.
 */
namespace question_11
{
    int sumOfSquare(int n);
}

#endif